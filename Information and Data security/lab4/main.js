﻿"use strict";

const PHRASE = 'enemyattackstonight';

const KEY = [3, 1, 4, 5, 2];

function getBlocks (phrase, blockSize) {
	var blocks = [];

	var regex = new RegExp('.{1,' + blockSize + '}', 'g');

	if (phrase.length % blockSize != 0) {
		var tmp = '';
		for (var i = 0; i < blockSize - (phrase.length % blockSize); i++) {
			tmp += 'z';
		}
		phrase += tmp;
	}

	blocks = phrase.match(regex);

	return blocks;
}

function createArrays (phrase, key) {
	var arrays = [];

	var blockSize = key.length;

	var blocks = getBlocks(phrase, blockSize);

	for (var k = 0; k < blocks[0].length; k++) {
		arrays.push([]);
	}

	for (var i = 0; i < blocks.length; i++) {
		for (var j = 0; j < blocks[i].length; j++) {
			arrays[j].push(blocks[i][j]);
		}
		
	}

	return arrays;
}

function encryptShuffle (phrase, key) {
	var enryptedPhrase = "";
	var shuffeledArrays = [];

	var arrays = createArrays(phrase, key);

	for (var k = 0; k < arrays.length; k++) {
		shuffeledArrays.push([]);
	}

	for (var j = 0; j < arrays.length; j++) {
		shuffeledArrays[j] = arrays[key[j] - 1];
	}

	for (var i = 0; i < shuffeledArrays.length; i++) {
		enryptedPhrase += shuffeledArrays[i].join("");
	}

	return enryptedPhrase;
}

function createDecryptArrays (phrase, key) {
	var arrays = [];

	var blockSize = phrase.length / key.length;

	var blocks = getBlocks(phrase, blockSize);

	for (var k = 0; k < blocks.length; k++) {
		arrays.push([]);
	}

	for (var i = 0; i < blocks.length; i++) {
		arrays[i] = blocks[i].split("");		
	}

	return arrays;
}

function getInsertPositions (eKey) {
	var map = {};

	for (var i = 0; i < eKey.length; i++) {
		map[eKey[i]] = i + 1;
	}

	return Object.values(map);
}

function decryptShuffle (phrase, key) {
	var decryptedPhrase = "";
	var shuffeledArrays = [];

	var arrays = createDecryptArrays(phrase, key);
	var insertPostitions = getInsertPositions(key);

	var blockSize = phrase.length / key.length;

	for (var k = 0; k < arrays.length; k++) {
		shuffeledArrays.push([]);
	}

	for (var j = 0; j < arrays.length; j++) {
		shuffeledArrays[j] = arrays[insertPostitions[j] - 1];
	}

	for (var i = 0; i < blockSize; i++) {
		for (var j = 0; j < shuffeledArrays.length; j++) {
			decryptedPhrase += shuffeledArrays[j][i];
		}
	}

	return decryptedPhrase;
}

var encryptedPhrase = encryptShuffle(PHRASE, KEY);

console.log(encryptedPhrase);

console.log(decryptShuffle(encryptedPhrase, KEY));