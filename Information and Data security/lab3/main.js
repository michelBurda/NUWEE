﻿"use strict";

const KEY_WORD = 'МОВА';

const ALFABET = [
	'А',
	'Б',
	'В',
	'Г',
	'Д',
	'Е',
	'Є',
	'Ж',
	'З',
	'И',
	'І',
	'Ї',
	'Й',
	'К',
	'Л',
	'М',
	'Н',
	'О',
	'П',
	'Р',
	'С',
	'Т',
	'У',
	'Ф',
	'Х',
	'Ц',
	'Ч',
	'Ш',
	'Щ',
	'Ь',
	'Ю',
	'Я',
	'_'
];

const PHRASE = 'ЗАХИСТ_ІНФОРМАЦІЇ';

function getkeyWordedPhrase(phrase, keyWord) {
	var keyWordedPhrase = '';
	var j = 0;

	for (var i = 0; i < phrase.length; i++) {
		keyWordedPhrase += keyWord[j];
		j++;
		if (j >= keyWord.length) { j = 0;}
	}

	return keyWordedPhrase;
}

console.log(getkeyWordedPhrase(PHRASE, KEY_WORD));

function getKeyArrays(keyWord, alfabet) {
	var keyArrays = [];

	for (var i = 0; i < keyWord.length; i++) {
		var indexOfCurrentSymbol = alfabet.indexOf(keyWord[i]);
		keyArrays[i] = [];

		for (var ii = indexOfCurrentSymbol; ii < alfabet.length; ii++) {
			keyArrays[i].push(alfabet[ii]);
		}
		for (var ii = 0; ii < indexOfCurrentSymbol; ii++) {
			keyArrays[i].push(alfabet[ii]);
		}
	}

	return keyArrays;
}

console.log(getKeyArrays(KEY_WORD, ALFABET));

function encryptVigenere(phrase, keyWord, alfabet) {
	var encrytpedPhrase = '';

	var keyWordedPhrase = getkeyWordedPhrase(phrase, keyWord);
	var keyArrays = getKeyArrays(keyWord, alfabet);

	for (var i = 0; i < phrase.length; i++) {
		var symbol 					= phrase[i];
		var keyWordedPhraseSymbol 	= keyWordedPhrase[i];
		var keyArraysIndex 			= keyWord.indexOf(keyWordedPhraseSymbol); 
		var alfabetInex 			= alfabet.indexOf(symbol);

		encrytpedPhrase += keyArrays[keyArraysIndex][alfabetInex];
	}

	return encrytpedPhrase;
}

function decryptVigenere(phrase, keyWord, alfabet) {
	var decryptedPhrase = '';

	var keyWordedPhrase = getkeyWordedPhrase(phrase, keyWord);
	var keyArrays 		= getKeyArrays(keyWord, alfabet);

	for (var i = 0; i < phrase.length; i++) {
		var symbol 					= phrase[i];
		var keyWordedPhraseSymbol 	= keyWordedPhrase[i];
		var keyArraysIndex 			= keyWord.indexOf(keyWordedPhraseSymbol); 
		var keyArraysSymbolIndex 	= keyArrays[keyArraysIndex].indexOf(symbol);

		decryptedPhrase += alfabet[keyArraysSymbolIndex];
	}

	return decryptedPhrase;
}


var encrytpedPhrase = encryptVigenere(PHRASE, KEY_WORD, ALFABET);
console.log(encrytpedPhrase);

console.log(decryptVigenere(encrytpedPhrase, KEY_WORD, ALFABET));