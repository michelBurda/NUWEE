﻿const KEY = [
	['L', 'G', 'D', 'B', 'A'],
	['Q', 'M', 'H', 'E', 'C'],
	['U', 'R', 'N', 'I', 'F'],
	['X', 'V', 'S', 'O', 'K'],
	['Z', 'Y', 'W', 'T', 'P']
];

const KEY1 = [
	['Ч', ' ', 'В', 'І', 'П'],
	['О', 'К', 'Й', 'Д', 'У'],
	['Г', 'Ш', 'З', 'Є', 'Ф'],
	['Л', 'Ї', 'Х', 'А', ','],
	['Ю', 'Р', 'Ж', 'Щ', 'Н'],
	['Ц', 'Б', 'И', 'Т', 'Ь'],
	['.', 'С', 'Я', 'М', 'Е']
];

const KEY2 = [
	['Е', 'Л', 'Ц', 'Й', 'П'],
	['.', 'Х', 'Ї', 'А', 'Н'],
	['Ш', 'Д', 'Є', 'К', 'С'],
	['І', ' ', 'Б', 'Ф', 'У'],
	['Я', 'Т', 'И', 'Ч', 'Г'],
	['М', 'О', ',', 'Ж', 'Ь'],
	['В', 'Щ', 'З', 'Ю', 'Р']
];

function cropForPleifer(str) {
	var result = [];

	// removing spaces from string given
	str = str.replace(/\s/g, '');
	
	for(var i = 0; i <= str.length - 1; ) {
		if (str[i] !== str[i+1]) {
			if (typeof str[i+1] !== 'undefined'){
				result.push(str[i] + str[i + 1]);
			} else {
				// add special character if str lenght is not even
				result.push(str[i] + 'X');
			}
			i += 2;
		} else {
			result.push(str[i] + 'X');
			i ++;
		}
	}

	return result;
}

function cryptPleifer(pairArray, key) {
	var keyRowsCount = key.length;
	var keyColsCount = key[0].length; 

	var result = '';

	for(var i = 0; i < pairArray.length; i++) {
		
		// Create obj form letter
		var pair = [];
		for(var l = 0; l < pairArray[i].length; l++) {
			pair.push({
				value: pairArray[i][l],
				row: -1,
				col: -1
			});
		}
			
		
		for(var c = 0; c < pair.length; c++) {
			// Fill obj information	
			for(var k = 0; k < key.length; k++) {
				var symbolCol = key[k].indexOf(pair[c].value);
				
				if (symbolCol > -1) {
					pair[c].row = k;
					pair[c].col = symbolCol;

					//console.log(pair[c]);
				}
			}
		}	

		// Processing
		if (pair[0].row == pair[1].row) 
		{
			var row = pair[0].row;
			var col_0 = (pair[0].col + 1) < keyColsCount ? pair[0].col + 1 : 0;
			var col_1 = (pair[1].col + 1) < keyColsCount ? pair[1].col + 1 : 0;

			result += key[row][col_0] + key[row][col_1];
		}
		else if (pair[0].col == pair[1].col) 
		{
			var col = pair[0].col;
			var row_0 = (pair[0].row + 1) < keyRowsCount ? pair[0].row + 1 : 0;
			var row_1 = (pair[1].row + 1) < keyRowsCount ? pair[1].row + 1 : 0;

			result += key[row_0][col] + key[row_1][col];
		}
		else 
		{
			var row_0 = pair[0].row;
			var col_0 = pair[1].col;
			var row_1 = pair[1].row;
			var col_1 = pair[0].col;
			result += key[row_0][col_0] + key[row_1][col_1];
		}
	}

	return result;
}

function decryptPleifer(pairArray, key) {
	var keyRowsCount = key.length;
	var keyColsCount = key[0].length; 

	var result = '';

	for(var i = 0; i < pairArray.length; i++) {
		
		// Create obj form letter
		var pair = [];
		for(var l = 0; l < pairArray[i].length; l++) {
			pair.push({
				value: pairArray[i][l],
				row: -1,
				col: -1
			});
		}
			
		
		for(var c = 0; c < pair.length; c++) {
			// Fill obj information	
			for(var k = 0; k < key.length; k++) {
				var symbolCol = key[k].indexOf(pair[c].value);
				
				if (symbolCol > -1) {
					pair[c].row = k;
					pair[c].col = symbolCol;

					//console.log(pair[c]);
				}
			}
		}	

		// Processing
		if (pair[0].row == pair[1].row) 
		{
			var row = pair[0].row;
			var col_0 = (pair[0].col - 1) >= 0 ? pair[0].col - 1 : keyColsCount - 1;
			var col_1 = (pair[1].col - 1) >= 0 ? pair[1].col - 1 : keyColsCount - 1;

			result += key[row][col_0] + key[row][col_1];
		}
		else if (pair[0].col == pair[1].col) 
		{
			var col = pair[0].col;
			var row_0 = (pair[0].row - 1) >= 0 ? pair[0].row - 1 : keyRowsCount - 1;
			var row_1 = (pair[1].row - 1) >= 0 ? pair[1].row - 1 : keyRowsCount - 1;

			result += key[row_0][col] + key[row_1][col];
		}
		else 
		{
			var row_0 = pair[0].row;
			var col_0 = pair[1].col;
			var row_1 = pair[1].row;
			var col_1 = pair[0].col;
			result += key[row_0][col_0] + key[row_1][col_1];
		}
	}

	return result;
}

var encrypted = cryptPleifer(cropForPleifer('HELLO'), KEY);

console.log(encrypted);

console.log(decryptPleifer(cropForPleifer(encrypted), KEY));

function cropForQuadrate(str) {
	var result = [];
	
	for(var i = 0; i <= str.length - 1; i += 2) {
		if (typeof str[i+1] !== 'undefined'){
			result.push(str[i] + str[i + 1]);
		} else {
			// add special character if str lenght is not even
			result.push(str[i] + ' ');
		}
	}

	return result;
}

function cryptQuadrate(pairArray, key1, key2) {
	var keyRowsCount = key1.length;
	var keyColsCount = key1[0].length; 

	var result = '';

	for(var i = 0; i < pairArray.length; i++) {
		
		// Create obj form letter
		var pair = [];
		for(var l = 0; l < pairArray[i].length; l++) {
			pair.push({
				value: pairArray[i][l],
				row: -1,
				col: -1
			});
		}

		// Fill obj information	
		for(var k = 0; k < key1.length; k++) {
			var symbolCol = key1[k].indexOf(pair[0].value);
			
			if (symbolCol > -1) {
				pair[0].row = k;
				pair[0].col = symbolCol;
			}
		}

		for(var k = 0; k < key2.length; k++) {
			var symbolCol = key2[k].indexOf(pair[1].value);
			
			if (symbolCol > -1) {
				pair[1].row = k;
				pair[1].col = symbolCol;
			}
		}

		// Processing
		if (pair[0].row == pair[1].row) 
		{
			var row_0 = pair[0].row;
			var col_0 = pair[1].col;
			var row_1 = pair[1].row;
			var col_1 = pair[0].col;
			result += key2[row_1][col_1] + key1[row_0][col_0];
		}
		else
		{
			var row_0 = pair[0].row;
			var col_0 = pair[1].col;
			var row_1 = pair[1].row;
			var col_1 = pair[0].col;
			result += key2[row_0][col_0] + key1[row_1][col_1];
		}
	}

	return result;
}

var encryptedQuadrate = cryptQuadrate(cropForQuadrate('ПРИЇЗДЖАЮ ШОСТОГО'), KEY1, KEY2);

console.log(encryptedQuadrate);

console.log(cryptQuadrate(cropForQuadrate(encryptedQuadrate), KEY2, KEY1));