tp = 1;
tk = 2;
x0 = 0.1;
x1 = -0.8;

[t,x] = ode45('l1b', [tp tk], -0.6);
[tt, xx] = ode45('l1b', [tp tk], x0);
[ttt, xxx] = ode45('l1b', [tp tk], x1);

plot(t, x, tt, xx, '+', ttt, xxx, '*');

axis([0.8 tk -1.5 0.5]);

grid