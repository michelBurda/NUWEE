﻿"use strict";

const P = 23;
const G = 5;

const M = 5;

const X = 18;

function generatePublicKey(g, x, p) {
	return expmod(g, x, p);
}

function getK(y, p) {
	var k = 0;

    for (var i = 2; i < p; i++)
    {
        var tmp = NSD(p - 1, i);
        if (tmp == 1)
        {
            k = i;
            break;
        }
    }

    return k;
}

function getA(g, k, p) {
	return expmod(g, k, p);
}

function getB(a, x, k, p, m) {
    for (var b = 2; b < p; b++) {
        if ((x * a + k * b) % (p - 1) === m)
            return b;
    }

    return 0;
}

function checkIntegrity(y, a, b, p, g, m) {
    var left1 = expmod(y, a, p);
    var left2 = expmod(a, b, p);;
    var right = expmod(g, m, p);

    return left1 * left2 === right;
}

function expmod(base, exp, mod){
	if (!exp) {
		return 1;
	}

	if (exp % 2 === 0){
		return Math.pow(expmod(base, (exp / 2), mod), 2) % mod;
	} else {
		return (base * expmod(base, (exp - 1), mod)) % mod;
	}
}

function generateRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

function NSD(a, b)
{
    if (a === b) {
        return a;
    } else if (a > b) {
        return NSD(a - b, b);
    } else {
        return NSD(b - a, a);
    }
}

var y = generatePublicKey(G, X, P);
console.log(y);

var k = getK(y, P);
console.log(k);

var a = getA(G, k, P);
console.log(a);

var b = getB(a, X, k, P, M);
console.log(b);

console.log(checkIntegrity(y, a, b, P, G, M));