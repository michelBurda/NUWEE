﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace LabWork2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double ComputeDeltaPhi(double phiUpper, double phiDown, int n)
        {
            return (phiUpper - phiDown) / n;
        }

        private double ComputeDeltaPsi(double Q, int m)
        {
            return Q / m;
        }

        private void FillPhiMatrix(double[,] phi, double phiDown, double deltaPhi, double d)
        {
            phi[0, 0] = phiDown;

            for (int i = 1; i < phi.GetLength(0); i++)
            {
                phi[i, 0] = phi[i - 1, 0] + d * deltaPhi;
            }

            for (int j = 1; j < phi.GetLength(1); j++)
            {
                phi[0, j] = phiDown;
            }

            for (int i = 1; i < phi.GetLength(0); i++)
            {
                for (int j = 1; j < phi.GetLength(1); j++)
                {
                    phi[i, j] = phi[i - 1, j] + d * deltaPhi;
                }
            }
        }

        private void FillPsiMatrix(double[,] psi, double deltaPsi)
        {
            psi[0, 0] = 0;

            for (int i = 1; i < psi.GetLength(0); i++)
            {
                psi[i, 0] = psi[i - 1, 0];
            }

            for (int j = 1; j < psi.GetLength(1); j++)
            {
                psi[0, j] = j * deltaPsi;
            }

            for (int i = 1; i < psi.GetLength(0); i++)
            {
                for (int j = 1; j < psi.GetLength(1); j++)
                {
                    psi[i, j] = psi[i - 1, j];
                }
            }
        }

        private double ComputeX(double phi, double psi)
        {
            return Math.Sqrt((phi + Math.Sqrt(Math.Pow(phi, 2) + Math.Pow(psi, 2))) / 2);
        }

        private double ComputeY(double phi, double psi)
        {
            return Math.Sqrt((phi + Math.Sqrt(Math.Pow(phi, 2) + Math.Pow(psi, 2))) / 2 - phi);
        }

        private void FillX(double[,] x, double[,] phi, double[,] psi)
        {
            for (int i = 0; i < x.GetLength(0); i++)
            {
                for (int j = 0; j < x.GetLength(1); j++)
                {
                    x[i, j] = ComputeX(phi[i, j], psi[i, j]);
                }
            }
        }

        private void FillY(double[,] y, double[,] phi, double[,] psi)
        {
            for (int i = 0; i < y.GetLength(0); i++)
            {
                for (int j = 0; j < y.GetLength(1); j++)
                {
                    y[i, j] = ComputeY(phi[i, j], psi[i, j]);
                }
            }
        }


        private void ShowGraphs_Click(object sender, EventArgs e)
        {
            const int n = 6;
            const int m = 4;

            const double Q = 4;

            const double d = 5;

            const double phiUpper = 2;
            const double phiDown = 1;

            double deltaPhi = ComputeDeltaPhi(phiUpper, phiDown, n);
            double deltaPsi = ComputeDeltaPsi(Q, m);

            double[,] phi = new double[n + 1, m + 1];
            double[,] psi = new double[n + 1, m + 1];

            FillPhiMatrix(phi, phiDown, deltaPhi, d);
            FillPsiMatrix(psi, deltaPsi);

            int counter = 0;

            for (int i = 0; i <= n; i++)
            {


                chart1.Series.Add(counter.ToString());

                chart1.Series[counter.ToString()].ChartType = SeriesChartType.Spline;

                for (int j = 0; j <= m; j++)
                {
                    chart1.Series[counter.ToString()].Points.AddXY(phi[i, j], psi[i, j]);
                }
                ++counter;
            }

            for (int j = 0; j <= m; j++)
            {
                ++counter;
                chart1.Series.Add(counter.ToString());
                chart1.Series[counter.ToString()].ChartType = SeriesChartType.Spline;
                for (int i = 0; i <= n; i++)
                {
                    chart1.Series[counter.ToString()].Points.AddXY(phi[i, j], psi[i, j]);
                }
            }


            double[,] x = new double[n + 1, m  +1];
            double[,] y = new double[n + 1, m + 1];

            FillX(x, phi, psi);
            FillY(y, phi, psi);

            counter = 0;
            for (int i = 0; i <= n; i++)
            {


                chart2.Series.Add(counter.ToString());

                chart2.Series[counter.ToString()].ChartType = SeriesChartType.Spline;

                for (int j = 0; j <= m; j++)
                {
                    chart2.Series[counter.ToString()].Points.AddXY(x[i, j] * 100 , y[i, j] * 100);
                }
                ++counter;
            }

            for (int j = 0; j <= m; j++)
            {
                ++counter;
                chart2.Series.Add(counter.ToString());
                chart2.Series[counter.ToString()].ChartType = SeriesChartType.Spline;
                for (int i = 0; i <= n; i++)
                {
                    chart2.Series[counter.ToString()].Points.AddXY(x[i, j] * 100 , y[i, j] * 100);
                }
            }
        }
    }
}
