﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApplication15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double fi1, fi2, Q;
        double dfi, dpsi;
        int m, n;
        double eps;
        double lyamda;

        double[] A = new double[2];
        double[] B = new double[2];
        double[] C = new double[2];
        double[] D = new double[2];

        double[,] x;
        double[,] y;
        double[,] xOld;
        double[,] yOld;

        //1.Start-------------------------------------------------------------------
        private double g1(double x, double y)
        {
            return x * x - y * y - 4;
        }
        private double g2(double x, double y)
        {
            return x * x - y * y - 1;
        }
        private double f1(double x, double y)
        {
            return y;
        }
        private double f2(double x, double y)
        {
            return x * y - 2;
        }
        private double g1x(double x, double y)
        {
            return 2 * x;
        }
        private double g2x(double x, double y)
        {
            return 2 * x;
        }
        private double f1x(double x, double y)
        {
            return 0;
        }
        private double f2x(double x, double y)
        {
            return y;
        }
        private double g1y(double x, double y)
        {
            return - 2 * y;
        }
        private double g2y(double x, double y)
        {
            return -2 * y;
        }
        private double f1y(double x, double y)
        {
            return 1;
        }
        private double f2y(double x, double y)
        {
            return x;
        }

        private void Init()
        {
            fi1 = 1;
            fi2 = 4;
            m = 20;
            n = 27;
            eps = 0.001;
            //1.End-----------------------------------------------------------------

            double tmp;
            double eps2 = 0.001;

            dfi = (fi2 - fi1) / n;

            x = new double[n + 1, m + 1];
            y = new double[n + 1, m + 1];
            xOld = new double[n + 1, m + 1];
            yOld = new double[n + 1, m + 1];

            //2.Start-----------------------------------------------------------------
            A[0] = 2;
            A[1] = 0;
            B[0] = 1;
            B[1] = 0;
            C[0] = Math.Sqrt(0.5 + Math.Sqrt(17) / 2);                                  
            C[1] = (Math.Sqrt(17) / 4 - 0.25) * Math.Sqrt(0.5 * (1 + Math.Sqrt(17)));  
            D[0] = Math.Sqrt(2 * (1 + Math.Sqrt(2)));                                  
            D[1] = (Math.Sqrt(2) - 1) * Math.Sqrt(2 * (1 + Math.Sqrt(2)));
            //2.End-----------------------------------------------------------------        

            //3.Start-----------------------------------------------------------------
            dfi = (fi2 - fi1) / n;
            //3.End-----------------------------------------------------------------

            //4.Start-----------------------------------------------------------------
            //AB
            for (int j = 0; j <= m; ++j)
            {
                x[0, j] = A[0] - j * (A[0] - B[0]) / m;
                do
                {
                    tmp = y[0, j];
                    y[0, j] = y[0, j] - f1(x[0, j], y[0, j]) / f1y(x[0, j], y[0, j]);
                } while (Math.Abs(tmp - y[0, j]) > eps2);
            }
            //CD
            for (int j = 0; j <= m; ++j)
            {
                x[n, j] = D[0] + j * (C[0] - D[0]) / m;
                do
                {
                    tmp = y[n, j];
                    y[n, j] = y[n, j] - f2(x[n, j], y[n, j]) / f2y(x[n, j], y[n, j]);
                } while (Math.Abs(tmp - y[n, j]) > eps2);
            }
            //AD
            for (int i = 0; i <= n; ++i)
            {
                y[i, 0] = A[1] + i * (D[1] - A[1]) / n;
                x[i, 0] = 1;
                do
                {
                    tmp = x[i, 0];
                    x[i, 0] = x[i, 0] - g1(x[i, 0], y[i, 0]) / g1x(x[i, 0], y[i, 0]);
                } while (Math.Abs(tmp - x[i, 0]) > eps2);
            }
            //BC
            for (int i = 0; i <= n; ++i)
            {
                y[i, m] = B[1] + i * (C[1] - B[1]) / n;
                x[i, m] = 1;
                do
                {
                    tmp = x[i, m];
                    x[i, m] = x[i, m] - g2(x[i, m], y[i, m]) / g2x(x[i, m], y[i, m]);
                } while (Math.Abs(tmp - x[i, m]) > eps2);
            }
            //inside
            for (int i = 1; i < n; ++i)
            {
                for (int j = 1; j < m; ++j)
                {
                    x[i, j] = (x[0, j] + x[n, j] + x[i, 0] + x[i, m]) / 4;
                    y[i, j] = (y[0, j] + y[n, j] + y[i, 0] + y[i, m]) / 4;
                }
            }
            //4.End-----------------------------------------------------------------
        }

        private double Lyamda()
        {
            double tmp = 0;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    tmp += (Math.Sqrt(Math.Pow(x[i, j + 1] - x[i, j], 2) + Math.Pow(y[i, j + 1] - y[i, j], 2))
                        + Math.Sqrt(Math.Pow(x[i + 1, j + 1] - x[i + 1, j], 2) + Math.Pow(y[i + 1, j + 1] - y[i + 1, j], 2)))
                        / (Math.Sqrt(Math.Pow(x[i + 1, j] - x[i, j], 2) + Math.Pow(y[i + 1, j] - y[i, j], 2))
                        + Math.Sqrt(Math.Pow(x[i + 1, j + 1] - x[i, j + 1], 2) + Math.Pow(y[i + 1, j + 1] - y[i, j + 1], 2)));
                }
            }
            return tmp / (n * m);
        }

        private void UtoVn()
        {
            double lyamdaKv = lyamda * lyamda;
            for (int i = 1; i < n; ++i)
            {
                for (int j = 1; j < m; ++j)
                {
                    x[i, j] = (lyamdaKv * (x[i + 1, j] + x[i - 1, j]) + x[i, j + 1] + x[i, j - 1]) / 2 / (1 + lyamdaKv);
                    y[i, j] = (lyamdaKv * (y[i + 1, j] + y[i - 1, j]) + y[i, j + 1] + y[i, j - 1]) / 2 / (1 + lyamdaKv);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void UtoGran()
        {
            double tmp;
            double eps2 = 0.001;

            //AB
            for (int j = 1; j < m; ++j)
            {
                x[0, j] = x[1, j];
                do
                {
                    tmp = y[0, j];
                    y[0, j] = y[0, j] - f1(x[0, j], y[0, j]) / f1y(x[0, j], y[0, j]);
                } while (Math.Abs(tmp - y[0, j]) > eps2);
            }
            //AD
            for (int i = 1; i < n; ++i)
            {
                do
                {
                    tmp = x[i, 0];
                    x[i, 0] = x[i, 0] - (x[i, 0] * (Math.Sqrt(x[i, 0] * x[i, 0] - 4) - y[i, 1]) + Math.Sqrt(x[i, 0] * x[i, 0] - 4) * (x[i, 0] - x[i, 1])) /
                                        (2 * Math.Sqrt(x[i, 0] * x[i, 0] - 4) - y[i, 1] + 2 * x[i, 0] * x[i, 0] / Math.Sqrt(x[i, 0] * x[i, 0] - 4) - x[i, 0] * x[i, 1] / Math.Sqrt(x[i, 0] * x[i, 0] - 4));
                } while (Math.Abs(tmp - x[i, 0]) > eps2);
                do
                {
                    tmp = y[i, 0];
                    y[i, 0] = y[i, 0] - g1(x[i, 0], y[i, 0]) / g1y(x[i, 0], y[i, 0]);
                } while (Math.Abs(tmp - y[i, 0]) > eps2);
            }
            //BC
            for (int i = 1; i < n; ++i)
            {
                do
                {
                    tmp = x[i, m];
                    x[i, m] = x[i, m] - (x[i, m] * (Math.Sqrt(x[i, m] * x[i, m] - 1) - y[i, m - 1]) + Math.Sqrt(x[i, m] * x[i, m] - 1) * (x[i, m] - x[i, m - 1])) /
                                        (2 * Math.Sqrt(x[i, m] * x[i, m] - 1) - y[i, m - 1] + 2 * x[i, m] * x[i, m] / Math.Sqrt(x[i, m] * x[i, m] - 1) - x[i, m] * x[i, m - 1] / Math.Sqrt(x[i, m] * x[i, m] - 1));
                } while (Math.Abs(tmp - x[i, m]) > eps2);
                do
                {
                    tmp = y[i, m];
                    y[i, m] = y[i, m] - g2(x[i, m], y[i, m]) / g2y(x[i, m], y[i, m]);
                } while (Math.Abs(tmp - y[i, m]) > eps2);
            }
            //CD
            for (int j = 1; j < m; ++j)
            {
                do
                {
                    tmp = x[n, j];
                    x[n, j] = x[n, j] - f2(x[n, j], y[n, j]) / f2y(x[n, j], y[n, j]);
                } while (Math.Abs(tmp - x[n, j]) > eps2);

                do
                {
                    tmp = y[n, j];
                    y[n, j] = y[n, j] - f2(x[n, j], y[n, j]) / f2y(x[n, j], y[n, j]);
                } while (Math.Abs(tmp - y[n, j]) > eps2);
            }

        }

        private bool Umova()
        {
            double sum = 0;
            for (int j = 1; j < m; ++j)
            {
                sum += Math.Abs(x[0, j] - xOld[0, j]);
                sum += Math.Abs(y[0, j] - yOld[0, j]);
                sum += Math.Abs(x[n, j] - xOld[n, j]);
                sum += Math.Abs(y[n, j] - yOld[n, j]);
            }
            for (int i = 1; i < n; ++i)
            {
                sum += Math.Abs(x[i, 0] - xOld[i, 0]);
                sum += Math.Abs(y[i, 0] - yOld[i, 0]);
                sum += Math.Abs(x[i, m] - xOld[i, m]);
                sum += Math.Abs(y[i, m] - yOld[i, m]);
            }

            for (int i = 0; i <= n; ++i)
            {
                for (int j = 0; j <= m; ++j)
                {
                    xOld[i, j] = x[i, j];
                    yOld[i, j] = y[i, j];
                }
            }

            if (sum > eps)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Show()
        {
           

            int counter = 0;

            for (int i = 0; i <= n; i++)
            {


                chart1.Series.Add(counter.ToString());

                chart1.Series[counter.ToString()].ChartType = SeriesChartType.Spline;

                for (int j = 0; j <= m; j++)
                {
                    chart1.Series[counter.ToString()].Points.AddXY(x[i, j], y[i, j]);
                }
                ++counter;
            }
            for (int j = 0; j <= m; j++) 
            {


                chart1.Series.Add(counter.ToString());

                chart1.Series[counter.ToString()].ChartType = SeriesChartType.Spline;

                for (int i = 0; i <= n; i++)
                {
                    chart1.Series[counter.ToString()].Points.AddXY(x[i, j], y[i, j]);
                }
                ++counter;
            }

            label2.Text = Q.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Init();//1. - 4.

            do
            {
                //5.Start-----------------------------------------------------------------
                lyamda = Lyamda();
                //5.End-----------------------------------------------------------------
                //6.Start-----------------------------------------------------------------
                dpsi = lyamda * dfi;
                Q = m * dpsi;
                //6.End-----------------------------------------------------------------
                //7.Start-----------------------------------------------------------------
                UtoVn();
                //7.End-----------------------------------------------------------------
                //8.Start-----------------------------------------------------------------
                UtoGran();
                //8.End-----------------------------------------------------------------
                //9.Start-----------------------------------------------------------------
            } while (Umova() == false);
            //9.End-----------------------------------------------------------------

            //10.Start-----------------------------------------------------------------
            Show();
            //10.End-----------------------------------------------------------------

        }
    }
}
