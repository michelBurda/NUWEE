tp = 0;
tk = 3;
x0 = 0.3;
x1 = -0.1;

[t,x] = ode45('l1a', [0 tk], 0);
[tt, xx] = ode45('l1a', [tp tk], x0);
[ttt, xxx] = ode45('l1a', [tp tk], x1);

plot(t, x, tt, xx, '+', ttt, xxx, '*');

axis([0 tk -0.2 1]);

grid