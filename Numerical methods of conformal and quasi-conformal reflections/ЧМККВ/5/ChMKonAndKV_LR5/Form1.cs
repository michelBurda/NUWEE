﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ChMKonAndKV_LR5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int n, m;
        double a, b, a1, a2;

        double[,] x;
        double[,] y;

        double[,] x1;
        double[,] y1;

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            string abc = "";

            a = (double)double.Parse(textBox1.Text);
            b = (double)double.Parse(textBox2.Text);
            n = (int)int.Parse(textBox3.Text);
            m = (int)int.Parse(textBox4.Text);
            a1 = (double)double.Parse(textBox5.Text);
            a2 = (double)double.Parse(textBox6.Text);

            
            x = new double[n + 1, m + 1];
            y = new double[n + 1, m + 1];

            x1 = new double[n + 1, m + 1];
            y1 = new double[n + 1, m + 1];

            double h1 = a / n;
            double h2 = (a2 - a1) / n;
            double h3 = a1 / m;
            double h4 = (a - a2) / m;

            double b1 = 0;
            double k1 = b / a1;
            double k2 = -(b / (a - a2));
            double b2 = (a * b) / (a - a2);

            //граничні

            for (int i = 0; i <= n; i++) 
            {
                y[i, 0] = 0;
                x[i, 0] = i * h1;
                y[i, m] = b;
                x[i, m] = a1 + i * h2;
            }

            for (int j = 0; j <= m; j++) 
            {
                x[0, j] = j * h3;
                y[0, j] = k1 * x[0, j] + b1;
                x[n, j] = a - j * h4;
                y[n, j] = k2 * x[n, j] + b2;

            }

            //внутрішні

            for (int j = 1; j <= m - 1; j++)
            {
                for (int i = 1; i <= n - 1; i++)
                {
                    x[i, j] = x[0, j] + i * (x[n, j] - x[0, j]) / n;

                }
            }

            for (int i = 1; i <= n - 1; i++)
            {
                for (int j = 1; j <= m - 1; j++)
                {
                    y[i, j] = j * (y[i, m] - y[i, 0]) / m;
                }
            }

            listBox1.Items.Add("X:");

            for (int i = 0; i <= n; i++)
            {
                for (int j = 0; j <= m; j++)
                {
                    abc += x[i, j].ToString("0.00") + " ";
                }
                listBox1.Items.Add(abc);
                abc = "";
            }

            listBox1.Items.Add(" ");
            listBox1.Items.Add("Y:");

            for (int i = 0; i <= n; i++)
            {
                for (int j = 0; j <= m; j++)
                {
                    abc += y[i, j].ToString("0.00") + " ";
                }
                listBox1.Items.Add(abc);
                abc = "";
            }

            DrawGraph2(chart1);

            //перерахунок
                for (int i = 1; i <= n - 1; i++)
                {
                    y[i, 0] = 0;
                    x[i, 0] = (4 * x[i, 1] - x[i, 2]) * 0.333333333;
                    y[i, m] = b;
                    x[i, m] = (4* x[i, m - 1] - x[i, m -2]) * 0.3333333;
                }

                for (int j = 1; j <= m - 1; j++)
                {
                    x[0, j] = (-x[2, j] + 4 * x[1, j] - y[0, j + 1] + y[0, j - 1]) * 0.333333333;
                    y[0, j] = k1 * x[0, j] + b1;
                    x[n, j] = (-x[n - 2, j] + 4 * x[n - 1, j] + (y[n, j + 1] - y[n, j - 1])) * 0.333333333;
                    y[n, j] = k2 * x[n, j] + b2;

                }



                for (int j = 1; j <= m - 1; j++)
                {
                    for (int i = 1; i <= n - 1; i++)
                    {
                        x[i, j] = x[0, j] + i * (x[n, j] - x[0, j]) / n;

                    }
                }



                for (int i = 1; i <= n - 1; i++)
                {
                    for (int j = 1; j <= m - 1; j++)
                    {
                        y[i, j] = j * (y[i, m] - y[i, 0]) / m;
                    }
                }

                listBox1.Items.Add("X:");

                for (int i = 0; i <= n; i++)
                {
                     for (int j = 0; j <= m; j++)
                     {
                         abc += x[i, j].ToString("0.00") + " ";
                     }
                     listBox1.Items.Add(abc);
                     abc = "";
                }

                listBox1.Items.Add(" ");
                listBox1.Items.Add("Y:");

                for (int i = 0; i <= n; i++)
                {
                    for (int j = 0; j <= m; j++)
                    {
                        abc += y[i, j].ToString("0.00") + " ";
                    }
                    listBox1.Items.Add(abc);
                    abc = "";
                }

            DrawGraph2(chart2);

        }

        private void DrawGraph2(System.Windows.Forms.DataVisualization.Charting.Chart chart)
        { 
            chart.Series.Clear();

            for (int i = 0; i <= n; i++)
            {
                chart.Series.Add("Re" + i.ToString());
                chart.Series["Re" + i.ToString()].ChartType = SeriesChartType.Spline;
                chart.Series["Re" + i.ToString()].Color = Color.GreenYellow;

                for (int j = 0; j <= m; j++)
                {
                    chart.Series["Re" + i.ToString()].Points.AddXY(x[i, j], y[i, j]);
                }                
            }

            for (int j = 0; j <= m; j++)
            {
                chart.Series.Add("Im" + j.ToString());
                chart.Series["Im" + j.ToString()].ChartType = SeriesChartType.Spline;
                chart.Series["Im" + j.ToString()].Color = Color.Indigo;

                for (int i = 0; i <= n; i++)
                {
                    chart.Series["Im" + j.ToString()].Points.AddXY(x[i, j], y[i, j]);
                }
            }
        }
    }
}
