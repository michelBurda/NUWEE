function Z=f1(t,x);
Z(1)=-2*x(1)+x(2);
Z(2)=8*x(1)-4*x(2);
Z=Z';