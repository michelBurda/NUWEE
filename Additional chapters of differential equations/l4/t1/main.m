tp = 0;
tk = 2.5;
x0 = 0.3;
x1 = 0.1;

[t,x] = ode45('f1', [0 tk], 0.2);
[tt, xx] = ode45('f1', [tp tk], x0);
[ttt, xxx] = ode45('f1', [tp tk], x1);

plot(t, x, tt, xx, '+', ttt, xxx, '*');

axis([0 tk -0.2 1]);

grid