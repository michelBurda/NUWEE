tp = 0;
tk  = 5;

plusdelta  = -0.2;
minusdelta = 0.4;

[t,x]   = ode45('f1',[0, tk], [0,0] );
[t1,x1] = ode45('f1',[tp, tk], [0 + plusdelta, 0 - plusdelta]);
[t2,x2] = ode45('f1',[tp, tk], [0.1 0.2]);
plot(t, x, t1, x1, '+', t2, x2, '*');

grid;