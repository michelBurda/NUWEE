t0 = 0;
tmax = 3;

[t, x] = ode45('f1', [t0, tmax], [0, 0]);
[t1, x1] = ode45('f1', [t0, tmax], [0.1, -0.2]);
[t2, x2] = ode45('f1', [t0, tmax], [0.4, -0.3]);

plot(t, x, t1, x1, '+', t2, x2, '*');
grid;