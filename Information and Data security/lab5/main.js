﻿"use strict";

const PHRASE = 'CAB';

const P = 3;
const Q = 11;

const N = P * Q;

const FI = (P - 1) * (Q - 1);

const E = false;

const D = false;

const ALPHABET = [
	'A', 
	'B', 
	'C', 
	'D', 
	'E', 
	'F', 
	'G', 
	'H', 
	'I', 
	'J', 
	'K', 
	'L', 
	'M', 
	'N', 
	'O', 
	'P', 
	'Q', 
	'R', 
	'S', 
	'T', 
	'U', 
	'V', 
	'W', 
	'X', 
	'Y', 
	'Z'
];

var PUBLIC_KEY = {
	e: E,
	n: N
};

var SECRET_KEY = {
	d: D,
	n: N
};

if (!PUBLIC_KEY.e) {
    var e = FI;

    while(!isCoprime(e, FI)) {
      e = generateNumber(2, FI - 1);
    }

    PUBLIC_KEY.e = e;
}

if (!SECRET_KEY.d) {
	SECRET_KEY.d = findMultInverse(PUBLIC_KEY.e , FI);
}

function encryptRSA(phrase, publicKey) {
	var encryptedPhrase = [];

	for (var i = 0; i < phrase.length; i++) {
		encryptedPhrase.push(
			expmod(ALPHABET.indexOf(phrase[i]) + 1, publicKey.e, publicKey.n)
		);
	}

	return encryptedPhrase;
}

function decryptRSA(phrase, secretKey) {
	var decryptedPhrase = '';
	
	for (var i = 0; i < phrase.length; i++) {
		var symbolIndex = expmod(phrase[i], secretKey.d, secretKey.n);

		decryptedPhrase += ALPHABET[symbolIndex - 1];
	}

	return decryptedPhrase;
}

function expmod(base, exp, mod) {
	if (!exp) {
	  return 1;
	}

	if (exp % 2 === 0){
  		return Math.pow(expmod(base, (exp / 2), mod), 2) % mod;
	} else {
  		return (base * expmod(base, (exp - 1), mod)) % mod;
	}
}

function generateNumber(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isCoprime(a, b) {
	return gsd(a, b) === 1;
}

function gsd(a, b) {
	if (!b) {
		return a;
	}

	return gsd(b, a % b);
}

function findMultInverse(number, module) {
	let result = 1;

	while ((number * result) % module !== 1) {
		result += 1;

		if (result > module) {
			return null;
		}
	}

	return result;
}

var encryptedPhrase = encryptRSA(PHRASE, PUBLIC_KEY);

console.log(encryptedPhrase);

console.log(decryptRSA(encryptedPhrase, SECRET_KEY));