"use strict";

function min(arr) {	
	var min = 100000;
	var pos = -1;
	
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] != 'M' && min > arr[i]) {
			pos = i;
			min = arr[i];
		}
	}
	
	return {
		position: 	pos, 
		value: 		min
	};
}

function getCurrentMatrix (prev) {
	var current = [[], [], [], [], [], []];
	
	for (var i = 0; i < prev.length; i++) {
		for (var j = 0; j < prev.length; j++) {
			if (i == j) {
				current[i][j] = 0;
			} else {
				var arrToFindMin = [];
				for (var k = 0; k < prev.length; k++) {
					if (prev[i][k] != 'M' && prev[k][j] != 'M') {
						arrToFindMin.push(prev[i][k] + prev[k][j]);
					} else {
						arrToFindMin.push('M');
					}
				}
				
				current[i][j] = current[j][i] = min(arrToFindMin).value;
			}
		}
	}

	return current;
}

function isEquals(arr1, arr2) {	
	for (var i = 0; i < arr1.length; i++) {
		for (var j = 0; j < arr2.length; j++) {
			if (arr1[i][j] != arr2[i][j]) {
				return false;
			}
		}
	}
	
	return true;
}

function formatResult(steps, matrix) {
	var result = {
		path: '',
		longitude: 0
	};

	for (var i = 0; i < steps.length; i++) {
		result.path += 'x(' + steps[i] + ')';
		if (i < steps.length - 1) {
			result.path += ' -> ';

			result.longitude += matrix[steps[i] - 1][steps[i + 1] - 1];
		}
	}

	return result;	
}

function findWay(fr, to, baseMatrix, matrix, steps) {
	if (fr == to) {
		steps.push(to);

		return formatResult(steps, matrix);
	}
	
	//console.log('Trying to find the way');
	//console.log('x(' + fr + ') -> ' + '...' + ' -> x(' + to + ')');

	steps.push(fr);
	
	var arrToFindMin = [];
	for (var k = 0; k < baseMatrix.length; k++) {		
		if (baseMatrix[fr - 1][k] != 'M' && baseMatrix[fr - 1][k] != 0) {
			//console.log(baseMatrix[fr - 1][k] + ' ' + matrix[k][to - 1]);
			arrToFindMin.push(baseMatrix[fr - 1][k] + matrix[k][to - 1]);
		} else {
			arrToFindMin.push('M');
		}
	}
	
	//console.log(arrToFindMin);
	
	var minPos = min(arrToFindMin).position + 1;

	return findWay(minPos, to, baseMatrix, matrix, steps);
	
}

;(function main (){
	var c_prev = [[], [], [], [], [], []];
	var c_curr = [[], [], [], [], [], []];

	var count = 0;

	const C = [
		[0, 'M', 1, 'M', 1, 4],
		['M', 0, 'M', 2, 5, 2],
		[1, 'M', 0, 2, 'M', 1],
		['M', 2, 2, 0, 'M', 3],
		[1, 5, 'M', 'M', 0, 2],
		[4, 2, 1, 3, 2, 0]
	];
	
	c_curr = C;
	
	while (!isEquals(c_prev, c_curr)) {
		c_prev = c_curr;
		c_curr = getCurrentMatrix(c_prev);
		count++;
	}

	console.log('Base matrix:');
	console.log(C);

	console.log('Optimal matrix is C^' + count + ':');
	console.log(c_curr);

	var result = findWay(1, 2, C, c_curr, []);

	console.log('Optimal path is:');
	console.log(result.path);

	console.log('Optimal longitude is:');
	console.log(result.longitude);
	
})();

