﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int H1 = 5;
            int H2 = 0;
            int Ct = 730;
            int T1 = 30;
            
            double n = 14.0 / 24.0;
           
            double k = 1.7;
            int l = 50;
            int n1 = 20;
            int m1 = 20;
            int h = l / n1;
            double tau = 0.01;
            int lamda = 94;
            int p = 1004;
            int cp = 450;
            double[,] arr = new double[m1 + 1, n1 + 1];

            for (int i = 1; i <= n1; i++) {
                arr[0, i] = 30 * ((l - h) / l);
            }

            for (int j = 0; j <= m1; j++) {
                arr[j, 0] = T1;
            }

            double u = -k * (H2 - H1) / l;
            double eta = lamda / (1 + h * Math.Abs(p * cp * u) / (2 * lamda));
            double r_min = (-(p * cp * u) - Math.Abs((p * cp * u))) / 2;
            double r_plus = (-(p * cp * u) + Math.Abs((p * cp * u))) /2;
            double a = (tau / Ct) * (eta / (Math.Pow(h, 2)) - r_min / h);
            double b = (tau /Ct) * (eta / (Math.Pow(h, 2)) + r_plus / h);
            double c = 1 + (tau / Ct) * (2 * eta / (Math.Pow(h, 2)) + (r_plus - r_min) / h );
           
            double[] alpha = new double[n1 + 1];
            double[] beta = new double[n1 + 1];
            
            alpha[0] = 0;
            beta[0] = T1;

            for (int j = 1; j <= m1; j++) {
                for (int i = 0; i < n1; i++) {
                    alpha[i + 1] = b / (c - a * alpha[i]);
                    beta[i + 1] = (a * beta[i] + arr[j - 1, i]) / (c - alpha[i] * a);
                }

                for (int i = n1 - 1 ; i >= 0; i--) {
                    arr[j, i + 1] =  beta[i + 1] / (1 + lamda * alpha[i + 1]);
                }
            }

            for (int j = 0; j <= m1; j++) {
                for (int i = 0; i <= n1; i++) {
                    Console.Write(arr[j, i].ToString("F4") + " ");
                }
                Console.WriteLine();
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}