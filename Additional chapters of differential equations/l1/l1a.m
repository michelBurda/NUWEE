function f = l1a( t, x )
    f = (1 - t^2) * x;
end

