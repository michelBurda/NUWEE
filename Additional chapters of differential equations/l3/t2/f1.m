function z = f1(~,x)
a = -1;
b = -1;

z(1) = a * x(1) - cos(x(2)) + exp(b * x(3));
z(2) = b * sin(x(1)) + log(1 + a * x(2)) - x(1) * (x(3)^2);
z(3) = x(1)^2 * cos(x(3)) + b * x(2) + sin(a * x(3));

z = z';

