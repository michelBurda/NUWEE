﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int H1 = 10;
            int H2 = 0;

            int Cm = 40;
            int C0 = 5;
            int C1 = Cm;

            double D = D = 0.04;
            double n = 14.0 / 24.0;
            double gamma = 26 * Math.Pow(10, -5);

            double k = 1.7;
            int l = 50;
            int n1 = 20;
            int m1 = 20;
            int h = l / n1;
            double tau = 2;
            double[,] arr = new double[m1 + 1, n1 + 1];

            for (int i = 1; i < n1; i++) {
                arr[0, i] = C0;
            }

            for (int j = 0; j <= m1; j++) {
                arr[j, 0] = C1;
                arr[j, n1] = C0;
            }

            double u = -k * (H2 - H1) / l;
            double eta = D / (1 + h * Math.Abs(u) / (2 * D));
            double r_min = (-u - Math.Abs(u)) / 2;
            double r_plus = (-u + Math.Abs(u)) /2;
            double a = (tau / n) * (eta / (h * h )- r_min / h);
            double b = (tau / n) * (eta / (h * h) + r_plus / h);
            double c = 1 + (tau / n) * (2 * eta / (h * h) + (r_plus - r_min) / h + gamma);
            double f = tau * gamma * Cm / n;
            double[] alpha = new double[n1 + 1];
            double[] beta = new double[n1 + 1];
            
            alpha[0] = 0;
            beta[0] = Cm;

            for (int j = 1; j <= m1; j++) {
                for (int i = 0; i < n1; i++) {
                    alpha[i + 1] = b / (c - a * alpha[i]);
                    beta[i + 1] = (a * beta[i] + arr[j-1, i] + f) / (c - alpha[i] * a);
                }


                for (int i = n1 - 1; i > 0; i--) {
                    arr[j, i] = alpha[i+1] * arr[j, i+1] + beta[i+1];
                }
            }

            for (int j = 0; j <= m1; j++) {
                for (int i = 0; i <= n1; i++) {
                    Console.Write(arr[j, i].ToString("F4") + " ");
                }
                Console.WriteLine();
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}