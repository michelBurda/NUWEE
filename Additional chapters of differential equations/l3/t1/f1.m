function z = f1(~,x)
a = 2;
b = -1;

z(1) = -sin(x(1) + a*x(2));
z(2) = -b * x(1) + log(1 - x(2));
z = z';