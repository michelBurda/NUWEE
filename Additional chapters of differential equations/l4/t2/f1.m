function z = f1(~, x ) 
a = -2;
b = 1;
c = -1;
d = -2;

z(1) = a * x(1)^3 + b * x(2);
z(2) = c * x(1) + d * x(2)^5;

z=z';

