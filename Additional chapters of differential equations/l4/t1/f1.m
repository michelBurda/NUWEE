function z = f1(t, x ) 
a = -2;
m = 2;

z(1) = a * x^m + x^2*(cos(x^2) - 1);

z=z';

