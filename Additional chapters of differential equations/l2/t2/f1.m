function Z=l1(t,x);
Z(1)=-3*x(1)-4*x(2);
Z(2)=2*x(1)+x(2);
Z=Z';