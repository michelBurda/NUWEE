function z = l1b(y, x)
    z = - 1/x^2 - y/x;
end